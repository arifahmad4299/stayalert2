import 'package:flutter/material.dart';
import 'package:stayalert/helpers/constants.dart';
import 'package:stayalert/views/search_screen.dart';

Widget commonAppBar(
    {required Color shadowColor,
    required BuildContext screenContext,
    required bool profilePic}) {
  Color tempColor = Colors.white;

  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20), bottomRight: Radius.circular(20)),
      color: Colors.white,
      boxShadow: [
        new BoxShadow(
          offset: Offset(0, 5),
          // ignore: unnecessary_null_comparison
          color: shadowColor == null ? tempColor : shadowColor,
          blurRadius: 5,
        ),
      ],
    ),
    height: 100,
    child: Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          profilePic == true
              ? Container(
                  height: 50,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(2),
                      boxShadow: [
                        BoxShadow(color: Colors.grey, spreadRadius: 0.1)
                      ]),
                  child: Image.asset(
                    PROFILE_PIC,
                    height: 15,
                  ),
                )
              : InkWell(
                  child: Icon(Icons.arrow_back_ios),
                  onTap: () {
                    Navigator.pop(screenContext);
                  },
                ),
          Image.asset(
            STAY_ALERT_LOGO,
            height: 80,
            width: MediaQuery.of(screenContext).size.width / 2,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: InkWell(
              child: Icon(Icons.search),
              onTap: () {
                Navigator.of(screenContext).push(
                    MaterialPageRoute(builder: (context) => SearchScreen()));
              },
            ),
          )
        ],
      ),
    ),
  );
}
