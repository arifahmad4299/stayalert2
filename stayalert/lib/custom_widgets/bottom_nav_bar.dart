import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:stayalert/helpers/constants.dart';
import '../views/home_screen.dart';
import '../views/menu_screen.dart';
import '../views/my_account_screen.dart';

class BottomNavBarV2 extends StatefulWidget {
  @override
  _BottomNavBarV2State createState() => _BottomNavBarV2State();
}

class _BottomNavBarV2State extends State<BottomNavBarV2> {
  int currentIndex = 0;
  int _selectedIndex = 0;
  double _iconSize0 = 38;
  double _iconSize1 = 20;
  double _iconSize2 = 20;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _selectedIndex == 0
          ? MenuScreen()
          : _selectedIndex == 1
              ? HomeScreen()
              : MyAccountScreen(),
      bottomNavigationBar: CurvedNavigationBar(
        buttonBackgroundColor: Colors.black,
        color: Colors.grey.shade200,
        backgroundColor: Colors.transparent,
        items: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(
                  MENU_ICON,
                  height: _iconSize0,
                  color: Colors.amber,
                ),
              ),
              text0()
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Image.asset(
                  HOME_ICON,
                  height: _iconSize1,
                  color: Colors.amber,
                ),
              ),
              text1()
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(
                  PERSON_LOGO,
                  height: _iconSize2,
                  color: Colors.amber,
                ),
              ),
              text2()
            ],
          ),
        ],
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
          });

          if (index == 0) {
            setState(() {
              _iconSize0 = 38;
            });
          } else {
            setState(() {
              _iconSize0 = 25;
            });
          }

          if (index == 1) {
            setState(() {
              _iconSize1 = 40;
            });
          } else {
            setState(() {
              _iconSize1 = 25;
            });
          }

          if (index == 2) {
            setState(() {
              _iconSize2 = 38;
            });
          } else {
            setState(() {
              _iconSize2 = 25;
            });
          }

          print(_selectedIndex);
        },
      ),
    );
  }

  Widget text0() {
    if (_selectedIndex == 0) {
      return Container();
      // ignore: dead_code
      setState(() {});
    }
    return Text("Menu");
    // ignore: dead_code
    setState(() {});
  }

  Widget text1() {
    if (_selectedIndex == 1) {
      return Container();
      // ignore: dead_code
      setState(() {});
    }
    return Text("Home");
    // ignore: dead_code
    setState(() {});
  }

  Widget text2() {
    if (_selectedIndex == 2) {
      return Container();
      // ignore: dead_code
      setState(() {});
    }
    return Text("My Account");
    // ignore: dead_code
    setState(() {});
  }
}
