import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:stayalert/helpers/constants.dart';
import 'views/school_screen.dart';

void main() {
  runApp(StayAlert());
}

class StayAlert extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Stay Alert',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: AnimatedSplashScreen(
            duration: 3000,
            splash: Image.asset(
              STAY_ALERT_LOGO,
              width: 400,
            ),
            nextScreen: SchoolCodeScreen(),
            splashTransition: SplashTransition.fadeTransition,
            backgroundColor: Colors.white));
  }
}
