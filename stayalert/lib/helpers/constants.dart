const String COMMUNICATION_ERROR_MESSAGE = "Error During Communication: ";
const String INVALID_REQUEST_MESSAGE = "Invalid Request: ";
const String UNAUTHORISED_MESSAGE = "Unauthorised: ";
const String INVALID_INPUT_MESSAGE = "Invalid Input: ";

const String NOIDAIRY_DELIGHTS = "NoiDairy Delights";
const String NOIDAIRY_SLOGAN = "Don't worry we are here";
const String INPUT_FIELD_REQUIRED = "Input field must required!";
const String ENTER_MOBILE_NO = "Enter Mobile no.";
const String ENTER_VALID_NO = "Enter valid Mobile number";

const String SEND_OTP_BUTTON = "Send OTP";
const String SUBMIT_BUTTON = "Submit";

const double HEADING_FONT = 15;
const double TITLE_FONT = 14;
const double ICON_FONT = 18;

const String MILK_PRODUCT = "Milk Products";
const String NOIDAIRY_BOY = "assets/images/milk_logo.png";
const String NOIDAIRY_LOGO = "assets/images/noidairy_logo.png";

const String STAY_ALERT_LOGO = "assets/images/stay_alert_logo_black.png";
const String MENU_ICON = "assets/images/menu_icon.png";
const String HOME_ICON = "assets/images/home_icon.png";
const String PERSON_LOGO = "assets/images/person_logo.png";
const String PROFILE_PIC = "assets/images/profile_pic.png";

const String LOGIN_ICON = "assets/images/login_icon.png";
const String WELCOME_ICON = "assets/images/welcome_icon.png";
const String NOTICE_ICON = "assets/images/notice.png";
const String ATTENDANCE_ICON = "assets/images/attendance.png";
const String ASSIGNMENT_ICON = "assets/images/assignment.png";
const String TIME_TABLE_ICON = "assets/images/time_table.png";
const String RESULT_ICON = "assets/images/result.png";
const String REPORT_ICON = "assets/images/report.png";
const String FEE_PAYMENT_ICON = "assets/images/fee_payment.png";
const String LUNCH_ICON = "assets/images/lunch.png";
const String OTP_ICON = "assets/images/otp_logo.png";
const String FACEBOOK_ICON = "assets/images/facebook_logo.png";
const String TWITTER_ICON = "assets/images/twitter_logo.png";
const String INSTAGRAM_ICON = "assets/images/instagram_logo.png";
const String YOUTUBE_ICON = "assets/images/youtube_logo.png";
