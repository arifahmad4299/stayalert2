import 'package:flutter/material.dart';
import 'package:stayalert/custom_widgets/common_app_bar.dart';
import 'package:stayalert/helpers/constants.dart';

class ResultScreen extends StatefulWidget {
  @override
  _ResultScreenState createState() => _ResultScreenState();
}

class _ResultScreenState extends State<ResultScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          //height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              Container(
                  // Container app bar
                  height: 90,
                  width: MediaQuery.of(context).size.width,
                  child: commonAppBar(
                      shadowColor: Colors.grey.shade300,
                      screenContext: context,
                      profilePic: false)),
              FractionallySizedBox(
                  widthFactor: 0.90,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 24,
                      ),
                      Text("RESULT CARD",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: HEADING_FONT,
                          )),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        ///////////////
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                          ),
                          color: Colors.white,
                          boxShadow: [
                            new BoxShadow(
                              offset: Offset(0, 2),
                              color: Colors.grey.shade400,
                              blurRadius: 3,
                            ),
                          ],
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      SizedBox(
                                        width: 15,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Text('Result of the week',
                                              style: TextStyle(
                                                  fontSize: TITLE_FONT,
                                                  fontWeight: FontWeight.bold)),
                                          SizedBox(
                                            height: 30,
                                          ),
                                          Text('Total Marks',
                                              style: TextStyle(
                                                  fontSize: TITLE_FONT,
                                                  fontWeight: FontWeight.w500)),
                                          // SizedBox(width: 10),
                                          // Text('100',
                                          //     style: TextStyle(
                                          //         fontSize: 17,
                                          //         fontWeight: FontWeight.bold,
                                          //         color: Colors.blue)),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Text('Obtained Marks',
                                              style: TextStyle(
                                                  fontSize: TITLE_FONT,
                                                  fontWeight: FontWeight.w500)),
                                          //   SizedBox(width: 10),
                                          //   Text('70',
                                          //       style: TextStyle(
                                          //           fontWeight: FontWeight.bold,
                                          //           fontSize: 17,
                                          //           color: Colors.green.shade500))
                                          // ,
                                          // SizedBox(
                                          //   height: 20,
                                          // )
                                        ],
                                      ),
                                      Column(children: [
                                        SizedBox(
                                          height: 53,
                                        ),
                                        Text('100',
                                            style: TextStyle(
                                                fontSize: 17,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.blue)),
                                        SizedBox(
                                          height: 14,
                                        ),
                                        Text('70',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 17,
                                                color: Colors.green.shade500))
                                      ])
                                    ],
                                  ),
                                  // Icon(
                                  //   Icons.no_accounts,
                                  //   size: 50,
                                  // )
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Image.asset(REPORT_ICON,
                                        width: 90, height: 110),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    "View Result",
                                    style: TextStyle(
                                        fontSize: HEADING_FONT,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  // MaterialButton(
                                  //   onPressed: () {},
                                  //   color: Colors.white,
                                  //   textColor: Colors.black,
                                  //   child: Icon(
                                  //     Icons.arrow_forward,
                                  //     size: 24,
                                  //   ),
                                  //   padding: EdgeInsets.all(16),
                                  //   shape: CircleBorder(),
                                  // )
                                  SizedBox(
                                    width: 15,
                                  ),
                                  InkWell(
                                    onTap: () {},
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(40)),
                                        color: Colors.white,
                                        boxShadow: [
                                          new BoxShadow(
                                            offset: Offset(0, 2),
                                            color: Colors.grey.shade400,
                                            blurRadius: 3,
                                          ),
                                        ],
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Icon(
                                          Icons.arrow_forward,
                                          size: 30,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
