import 'package:flutter/material.dart';
import 'package:stayalert/helpers/constants.dart';
import '../custom_widgets/common_app_bar.dart';

class Announcement extends StatefulWidget {
  @override
  _AnnouncementState createState() => _AnnouncementState();
}

class _AnnouncementState extends State<Announcement> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              commonAppBar(
                  screenContext: context,
                  shadowColor: Colors.grey.shade300,
                  profilePic: false),
              SizedBox(
                height: 25,
              ),
              FractionallySizedBox(
                widthFactor: 0.90,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'NOTIFICATIONS',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: HEADING_FONT),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(20),
                        ),
                        color: Colors.white,
                        boxShadow: [
                          new BoxShadow(
                            spreadRadius: 2,
                            offset: Offset(0, 5),
                            color: Colors.grey.shade300,
                            blurRadius: 5,
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Everyday English-French French',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: TITLE_FONT),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      'Conversation and Fun - Joe!',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: TITLE_FONT),
                                    ),
                                  ],
                                ),
                                Image.asset(NOTICE_ICON, width: 84, height: 85),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  '9 hrs',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: TITLE_FONT),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    'Never give up, ',
                                    style: TextStyle(fontSize: TITLE_FONT),
                                  ),
                                  Text(
                                    'know more',
                                    style: TextStyle(
                                        color: Colors.yellow[600],
                                        fontSize: TITLE_FONT),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    child: Icon(Icons.arrow_forward,
                                        color: Colors.black, size: 30.0),
                                    width: 50.0,
                                    height: 50.0,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 5,
                                          color: Colors.grey,
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
