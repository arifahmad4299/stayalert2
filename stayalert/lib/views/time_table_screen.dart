import 'package:flutter/material.dart';
import 'package:stayalert/custom_widgets/common_app_bar.dart';
import 'package:stayalert/helpers/constants.dart';
import 'package:stayalert/model/time_table_model.dart';

class TimeTableScreen extends StatefulWidget {
  @override
  _TimeTableScreenState createState() => _TimeTableScreenState();
}

class _TimeTableScreenState extends State<TimeTableScreen> {
  bool monDay = true,
      tuesDay = false,
      wednesDay = false,
      thursDay = false,
      friDay = false,
      saturDay = false;

  late List<TimeTableModel> mondayList = [
    TimeTableModel(
        subjectName: "Biology",
        time: "8:00am to 9:00am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Physics",
        time: "9:00am to 10:00am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Mathematics",
        time: "10:00am to 11:00am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Lunch Break",
        time: "11:00am to 11:30am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "English",
        time: "11:30am to 12:30pm",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Biology",
        time: "12:30pm to 1:30pm",
        standard: "9th",
        section: "B"),
  ];

  late List<TimeTableModel> tuesdayList = [
    TimeTableModel(
        subjectName: "Mathematics",
        time: "8:00am to 9:00am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Physics",
        time: "9:00am to 10:00am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Mathematics",
        time: "10:00am to 11:00am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Lunch Break",
        time: "11:00am to 11:30am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "English",
        time: "11:30am to 12:30pm",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Biology",
        time: "12:30pm to 1:30pm",
        standard: "9th",
        section: "B"),
  ];

  late List<TimeTableModel> wednesdayList = [
    TimeTableModel(
        subjectName: "Library",
        time: "8:00am to 9:00am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Physics",
        time: "9:00am to 10:00am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Mathematics",
        time: "10:00am to 11:00am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Lunch Break",
        time: "11:00am to 11:30am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "English",
        time: "11:30am to 12:30pm",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Biology",
        time: "12:30pm to 1:30pm",
        standard: "9th",
        section: "B"),
  ];

  late List<TimeTableModel> thursdayList = [
    TimeTableModel(
        subjectName: "Game",
        time: "8:00am to 9:00am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Physics",
        time: "9:00am to 10:00am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Mathematics",
        time: "10:00am to 11:00am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Lunch Break",
        time: "11:00am to 11:30am",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "English",
        time: "11:30am to 12:30pm",
        standard: "9th",
        section: "B"),
    TimeTableModel(
        subjectName: "Biology",
        time: "12:30pm to 1:30pm",
        standard: "9th",
        section: "B"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              Container(
                height: 90,
                width: MediaQuery.of(context).size.width,
                child: commonAppBar(
                    shadowColor: Colors.grey.shade300,
                    screenContext: context,
                    profilePic: false),
              ),
              SizedBox(height: 20),
              FractionallySizedBox(
                widthFactor: 0.90,
                child: Column(
                  children: [
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'TIME TABLE',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: HEADING_FONT),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(15),
                            bottomLeft: Radius.circular(15),
                            topRight: Radius.circular(15),
                            bottomRight: Radius.circular(15)),
                        border: Border.all(color: Colors.grey.shade300),
                      ),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                              onTap: () {
                                setState(() {
                                  monDay = true;
                                  tuesDay = false;
                                  wednesDay = false;
                                  thursDay = false;
                                  friDay = false;
                                  saturDay = false;
                                });
                              },
                              child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: monDay == true
                                          ? BorderRadius.all(
                                              Radius.circular(20))
                                          : BorderRadius.all(
                                              Radius.circular(0)),
                                      color: monDay == true
                                          ? Colors.black
                                          : Colors.transparent,
                                      boxShadow: [
                                        new BoxShadow(
                                          offset: Offset(0, 2),
                                          color: monDay == true
                                              ? Colors.greenAccent.shade100
                                              : Colors.transparent,
                                          blurRadius: 3,
                                        ),
                                      ]),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 11.0,
                                        right: 11.0,
                                        top: 7.0,
                                        bottom: 7.0),
                                    child: Text(
                                      'Mon',
                                      style: TextStyle(
                                          color: monDay == true
                                              ? Colors.white
                                              : Colors.black,
                                          fontSize: monDay == true ? 17 : 15),
                                    ),
                                  ))),
                          InkWell(
                              onTap: () {
                                setState(() {
                                  monDay = false;
                                  tuesDay = true;
                                  wednesDay = false;
                                  thursDay = false;
                                  friDay = false;
                                  saturDay = false;
                                });
                              },
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: tuesDay == true
                                      ? BorderRadius.all(Radius.circular(20))
                                      : BorderRadius.all(Radius.circular(0)),
                                  color: tuesDay == true
                                      ? Colors.black
                                      : Colors.transparent,
                                  boxShadow: [
                                    new BoxShadow(
                                      offset: Offset(0, 2),
                                      color: tuesDay == true
                                          ? Colors.greenAccent.shade100
                                          : Colors.transparent,
                                      blurRadius: 3,
                                    )
                                  ],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 13.0,
                                      right: 13.0,
                                      top: 7.0,
                                      bottom: 7.0),
                                  child: Text(
                                    'Tue',
                                    style: TextStyle(
                                        color: tuesDay == true
                                            ? Colors.white
                                            : Colors.black,
                                        fontSize: tuesDay == true ? 17 : 15),
                                  ),
                                ),
                              )),
                          InkWell(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              onTap: () {
                                setState(() {
                                  monDay = false;
                                  tuesDay = false;
                                  wednesDay = true;
                                  thursDay = false;
                                  friDay = false;
                                  saturDay = false;
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: wednesDay == true
                                      ? BorderRadius.all(Radius.circular(20))
                                      : BorderRadius.all(Radius.circular(0)),
                                  color: wednesDay == true
                                      ? Colors.black
                                      : Colors.transparent,
                                  boxShadow: [
                                    new BoxShadow(
                                      offset: Offset(0, 2),
                                      color: wednesDay == true
                                          ? Colors.greenAccent.shade100
                                          : Colors.transparent,
                                      blurRadius: 3,
                                    ),
                                  ],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 11.0,
                                      right: 11.0,
                                      top: 7.0,
                                      bottom: 7.0),
                                  child: Text(
                                    'Wed',
                                    style: TextStyle(
                                        color: wednesDay == true
                                            ? Colors.white
                                            : Colors.black,
                                        fontSize: wednesDay == true ? 17 : 15),
                                  ),
                                ),
                              )),
                          InkWell(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              onTap: () {
                                setState(() {
                                  monDay = false;
                                  tuesDay = false;
                                  wednesDay = false;
                                  thursDay = true;
                                  friDay = false;
                                  saturDay = false;
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: thursDay == true
                                      ? BorderRadius.all(Radius.circular(20))
                                      : BorderRadius.all(Radius.circular(0)),
                                  color: thursDay == true
                                      ? Colors.black
                                      : Colors.transparent,
                                  boxShadow: [
                                    new BoxShadow(
                                      offset: Offset(0, 2),
                                      color: thursDay == true
                                          ? Colors.greenAccent.shade100
                                          : Colors.transparent,
                                      blurRadius: 3,
                                    ),
                                  ],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 11.0,
                                      right: 11.0,
                                      top: 7.0,
                                      bottom: 7.0),
                                  child: Text(
                                    'Thu',
                                    style: TextStyle(
                                        color: thursDay == true
                                            ? Colors.white
                                            : Colors.black,
                                        fontSize: thursDay == true ? 17 : 15),
                                  ),
                                ),
                              )),
                          InkWell(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              onTap: () {
                                setState(() {
                                  monDay = false;
                                  tuesDay = false;
                                  wednesDay = false;
                                  thursDay = false;
                                  friDay = true;
                                  saturDay = false;
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: friDay == true
                                        ? BorderRadius.all(Radius.circular(19))
                                        : BorderRadius.all(Radius.circular(0)),
                                    color: friDay == true
                                        ? Colors.black
                                        : Colors.transparent,
                                    boxShadow: [
                                      new BoxShadow(
                                        offset: Offset(0, 2),
                                        color: friDay == true
                                            ? Colors.greenAccent.shade100
                                            : Colors.transparent,
                                        blurRadius: 3,
                                      ),
                                    ]),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 14.0,
                                      right: 14.0,
                                      top: 6.5,
                                      bottom: 6.5),
                                  child: Text(
                                    'Fri',
                                    style: TextStyle(
                                        color: friDay == true
                                            ? Colors.white
                                            : Colors.black,
                                        fontSize: friDay == true ? 18 : 16),
                                  ),
                                ),
                              )),
                          InkWell(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              onTap: () {
                                setState(() {
                                  monDay = false;
                                  tuesDay = false;
                                  wednesDay = false;
                                  thursDay = false;
                                  friDay = false;
                                  saturDay = true;
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: saturDay == true
                                      ? BorderRadius.all(Radius.circular(20))
                                      : BorderRadius.all(Radius.circular(0)),
                                  color: saturDay == true
                                      ? Colors.black
                                      : Colors.transparent,
                                  boxShadow: [
                                    new BoxShadow(
                                      offset: Offset(0, 2),
                                      color: saturDay == true
                                          ? Colors.greenAccent.shade100
                                          : Colors.transparent,
                                      blurRadius: 3,
                                    ),
                                  ],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 13.0,
                                      right: 13.0,
                                      top: 7.0,
                                      bottom: 7.0),
                                  child: Text(
                                    'Sat',
                                    style: TextStyle(
                                        color: saturDay == true
                                            ? Colors.white
                                            : Colors.black,
                                        fontSize: saturDay == true ? 17 : 15),
                                  ),
                                ),
                              )),
                        ],
                      ),
                    ),
                    // SizedBox(
                    //   height: 5,
                    // ),

                    Container(
                      height: MediaQuery.of(context).size.height / 1.5,
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount:
                              mondayList.length == 0 ? 0 : mondayList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return SingleChildScrollView(
                              child: Container(
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: 20,
                                    ),
                                    mondayList[index].subjectName ==
                                            "Lunch Break"
                                        ? _lunchBreak(mondayList[index].time)
                                        : monDay == true
                                            ? _classes(
                                                mondayList[index].subjectName,
                                                mondayList[index].standard,
                                                mondayList[index].section,
                                                mondayList[index].time)
                                            : tuesDay == true
                                                ? _classes(
                                                    tuesdayList[index]
                                                        .subjectName,
                                                    tuesdayList[index].standard,
                                                    tuesdayList[index].section,
                                                    tuesdayList[index].time)
                                                : wednesDay == true
                                                    ? _classes(
                                                        wednesdayList[index]
                                                            .subjectName,
                                                        wednesdayList[index]
                                                            .standard,
                                                        wednesdayList[index]
                                                            .section,
                                                        wednesdayList[index]
                                                            .time)
                                                    : thursDay == true
                                                        ? _classes(
                                                            thursdayList[index]
                                                                .subjectName,
                                                            thursdayList[index]
                                                                .standard,
                                                            thursdayList[index]
                                                                .section,
                                                            thursdayList[index]
                                                                .time)
                                                        : friDay == true
                                                            ? _classes(
                                                                mondayList[
                                                                        index]
                                                                    .subjectName,
                                                                mondayList[
                                                                        index]
                                                                    .standard,
                                                                mondayList[
                                                                        index]
                                                                    .section,
                                                                mondayList[
                                                                        index]
                                                                    .time)
                                                            : saturDay == true
                                                                ? _classes(
                                                                    mondayList[
                                                                            index]
                                                                        .subjectName,
                                                                    mondayList[
                                                                            index]
                                                                        .standard,
                                                                    mondayList[
                                                                            index]
                                                                        .section,
                                                                    mondayList[
                                                                            index]
                                                                        .time)
                                                                : Text(
                                                                    "Nothing to display")
                                  ],
                                ),
                              ),
                            );
                          }),
                    ),
                    // Lunch break //
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _lunchBreak(String timeDuration) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(10),
          bottomRight: Radius.circular(10),
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
        color: Colors.white,
        boxShadow: [
          new BoxShadow(
            offset: Offset(0, 2),
            color: Colors.grey.shade400,
            blurRadius: 3,
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Text('Lunch Break',
                        style: TextStyle(
                            fontSize: TITLE_FONT, fontWeight: FontWeight.w700)),
                    SizedBox(
                      height: 25,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text('Time:',
                            style: TextStyle(
                              fontSize: TITLE_FONT,
                            )),
                        SizedBox(width: 10),
                        Text(timeDuration,
                            style: TextStyle(
                                fontSize: TITLE_FONT,
                                color: Colors.black,
                                fontWeight: FontWeight.w500)),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: 20,
                    )
                  ],
                ),
                // Icon(
                //   Icons.no_accounts,
                //   size: 50,
                // )
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.asset(LUNCH_ICON, width: 90, height: 110),
                ),
              ],
            ),
            SizedBox(height: 5),
          ],
        ),
      ),
    );
  }

  Widget _classes(
      String subjectName, String? standard, String? section, String time) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(15)),
        color: Colors.white,
        boxShadow: [
          new BoxShadow(
            offset: Offset(0, 2),
            color: Colors.grey.shade400,
            blurRadius: 3,
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 12.0),
            child: Text(subjectName,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700)),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 13,
                    ),
                    Text('Time:',
                        style: TextStyle(
                          fontSize: TITLE_FONT,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    Text('Class:',
                        style: TextStyle(
                          fontSize: TITLE_FONT,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    Text('Section:',
                        style: TextStyle(
                          fontSize: TITLE_FONT,
                        )),
                  ],
                ),
                Column(children: [
                  SizedBox(
                    height: 13,
                  ),
                  Text(time,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
                  SizedBox(
                    height: 8,
                  ),
                  Text(standard.toString(),
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
                  SizedBox(
                    height: 8,
                  ),
                  Text(section.toString(),
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w600))
                ]),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                "Details",
                style: TextStyle(fontSize: TITLE_FONT),
              ),
              SizedBox(width: 13),
              InkWell(
                onTap: () {},
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(40)),
                    color: Colors.white,
                    boxShadow: [
                      new BoxShadow(
                        offset: Offset(0, 2),
                        color: Colors.grey.shade400,
                        blurRadius: 8,
                      ),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      Icons.arrow_forward,
                      size: 30,
                    ),
                  ),
                ),
              ),
              SizedBox(width: 8),
            ],
          ),
          SizedBox(height: 5),
        ],
      ),
      // ),
    );
  }
}
