import 'package:flutter/material.dart';
import 'package:stayalert/helpers/constants.dart';
import '../custom_widgets/bottom_nav_bar.dart';
import '../custom_widgets/common_app_bar.dart';

class Attendance extends StatefulWidget {
  @override
  _AttendanceState createState() => _AttendanceState();
}

class _AttendanceState extends State<Attendance> {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              commonAppBar(
                  shadowColor: Colors.grey.shade300,
                  screenContext: context,
                  profilePic: false),
              SizedBox(
                height: 25,
              ),
              FractionallySizedBox(
                widthFactor: 0.90,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'ATTENDANCE',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: HEADING_FONT),
                    ),
                    SizedBox(height: 22),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(20),
                        ),
                        color: Colors.white,
                        boxShadow: [
                          new BoxShadow(
                            spreadRadius: 2,
                            offset: Offset(0, 5),
                            color: Colors.grey.shade300,
                            blurRadius: 5,
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Afnan Sameer',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: TITLE_FONT),
                                ),
                                Row(
                                  children: [
                                    Text(
                                      'Check Out',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: TITLE_FONT),
                                    ),
                                    Icon(
                                      Icons.highlight_off,
                                      color: Colors.red,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(height: 4),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  'Time: 8:00 AM',
                                  style: TextStyle(fontSize: TITLE_FONT),
                                ),
                              ],
                            ),
                            SizedBox(height: 8),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  'Dated: 05-05-2021',
                                  style: TextStyle(fontSize: TITLE_FONT),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: size.width / 4,
                                  height: 10,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: Colors.red,
                                  ),
                                ),
                                Text(
                                  'Class 7th Section: B',
                                  style: TextStyle(fontSize: TITLE_FONT),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    'Never give up, ',
                                    style: TextStyle(fontSize: TITLE_FONT),
                                  ),
                                  Text(
                                    'know more',
                                    style: TextStyle(
                                        color: Colors.yellow[600],
                                        fontSize: TITLE_FONT),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    child: Icon(Icons.arrow_forward,
                                        color: Colors.black, size: 30.0),
                                    width: 50.0,
                                    height: 50.0,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 5,
                                          color: Colors.grey,
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
