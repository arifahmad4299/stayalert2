import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:flutter/material.dart';
import 'package:stayalert/views/announcement.dart';
import 'package:stayalert/views/assignment.dart';
import 'package:stayalert/views/attendance.dart';
import 'package:stayalert/views/fee_reminder_screen.dart';
import 'package:stayalert/views/login_screen.dart';
import 'package:stayalert/views/menu_screen.dart';
import 'package:stayalert/views/my_account_screen.dart';
import 'package:stayalert/views/time_table_screen.dart';
import '/custom_widgets/common_app_bar.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  List<String> added = [];
  String currentText = "";
  GlobalKey<AutoCompleteTextFieldState<String>> key = GlobalKey();

  _SearchScreenState() {
    textField = SimpleAutoCompleteTextField(
      key: key,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.only(left: 20),
          hintText: "Example: Result, Notices etc",
          border: InputBorder.none),
      controller: TextEditingController(),
      suggestions: suggestions,
      textChanged: (text) => currentText = text,
      clearOnSubmit: true,
      textSubmitted: (text) => setState(() {
        if (text != "") {
          switch (text) {
            case "Attendance":
              {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => Attendance()));
              }
              break;
            case "Notice and Announcements":
              {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => Announcement()));
              }
              break;
            case "Announcements":
              {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => Announcement()));
              }
              break;
            case "Notice":
              {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => Announcement()));
              }
              break;
            case "Assignments":
              {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => Assignment()));
              }
              break;
            case "Fee Payments":
              {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (context) => FeeReminderScreen()));
              }
              break;
            case "Time Table":
              {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => TimeTableScreen()));
              }
              break;
          }
        }
      }),
    );
  }

  List<String> suggestions = [
    "Attendance",
    "Notice and Announcements",
    "Announcements",
    "Notice",
    "Assignments",
    "Fee Payments",
    "Time Table",
    "Results",
  ];

  late SimpleAutoCompleteTextField textField;
  bool showWhichErrorText = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20)),
                    color: Colors.amber,
                    boxShadow: [
                      new BoxShadow(
                        offset: Offset(0, 5),
                        // ignore: unnecessary_null_comparison
                        color: Colors.grey.shade300,
                        blurRadius: 5,
                      ),
                    ],
                  ),
                  height: 100,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 5.0, right: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                            width: MediaQuery.of(context).size.width / 1.2,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20),
                                boxShadow: [
                                  BoxShadow(color: Colors.grey, spreadRadius: 1)
                                ]),
                            child: textField),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 7.0),
                            child: Icon(
                              Icons.close,
                              size: 30,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 30,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
