import 'package:flutter/material.dart';
import 'package:stayalert/helpers/constants.dart';
import '/custom_widgets/common_app_bar.dart';

class FeeReminderScreen extends StatefulWidget {
  @override
  _FeeReminderScreenState createState() => _FeeReminderScreenState();
}

class _FeeReminderScreenState extends State<FeeReminderScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Column(
              children: [
                commonAppBar(
                    shadowColor: Colors.grey.shade300,
                    screenContext: context,
                    profilePic: false),
                SizedBox(
                  height: 25,
                ),
                FractionallySizedBox(
                  widthFactor: 0.90,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'FEE REMINDER',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: HEADING_FONT),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(20),
                          ),
                          color: Colors.white,
                          boxShadow: [
                            new BoxShadow(
                              spreadRadius: 2,
                              offset: Offset(0, 5),
                              color: Colors.grey.shade300,
                              blurRadius: 5,
                            ),
                          ],
                        ),
                        child: Padding(
                          padding:
                              const EdgeInsets.only(top: 15.0, bottom: 15.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "FEE REMINDER",
                                      style: TextStyle(
                                          fontSize: HEADING_FONT,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Text(
                                      "Last Date:",
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Text("23/May/2021")
                                  ],
                                ),
                                SizedBox(
                                  width: MediaQuery.of(context).size.width / 5,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Status",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Container(
                                      child: Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: Text(
                                          "UnPaid",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ),
                                      decoration: BoxDecoration(
                                          color: Colors.red,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(6))),
                                    )
                                  ],
                                ),
                              ]),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
