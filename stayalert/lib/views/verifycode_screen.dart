import 'package:flutter/material.dart';
import 'package:stayalert/custom_widgets/bottom_nav_bar.dart';
import 'package:stayalert/helpers/constants.dart';
import '../views/home_screen.dart';

class OTPScreen extends StatefulWidget {
  @override
  _OTPScreenState createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  final verificationCode = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              color: Colors.white,
              child: Column(
                children: [
                  SizedBox(
                    height: 3,
                  ),
                  Center(
                    child: Image.asset(
                      STAY_ALERT_LOGO,
                      height: 80,
                      width: MediaQuery.of(context).size.width / 2,
                    ),
                  ),
                  SizedBox(
                    height: 100,
                  ),
                  Center(
                    child: Image.asset(
                      OTP_ICON,
                      height: 143,
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 1.66,
                    decoration: BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 70,
                        ),
                        Text(
                          "Please enter the Verification code",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: TITLE_FONT),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5)),
                          width: 165,
                          child: Center(
                            child: TextFormField(
                                controller: verificationCode,
                                showCursor: false,
                                textAlign: TextAlign.center,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                )),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            if (verificationCode.text.toString() == "" ||
                                verificationCode.text.isEmpty) {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(const SnackBar(
                                backgroundColor: Colors.red,
                                content: Text(
                                  'Please enter Valid code!',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ));
                            } else {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => BottomNavBarV2()));
                            }
                          },
                          child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.black,
                                  borderRadius: BorderRadius.circular(5)),
                              width: 165,
                              height: 42,
                              child: Center(
                                child: Text(
                                  "Verify Code",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: TITLE_FONT),
                                ),
                              )),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height / 9,
                        ),
                        Container(
                          height: 100,
                          child: Row(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width / 2,
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Column(
                                    children: [
                                      Text(
                                        "Please Like Us On!",
                                        style: TextStyle(color: Colors.black),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            FACEBOOK_ICON,
                                            height: 30,
                                          ),
                                          SizedBox(
                                            width: 3,
                                          ),
                                          Image.asset(
                                            TWITTER_ICON,
                                            height: 30,
                                          ),
                                          SizedBox(
                                            width: 3,
                                          ),
                                          Image.asset(
                                            INSTAGRAM_ICON,
                                            height: 30,
                                          ),
                                          SizedBox(
                                            width: 3,
                                          ),
                                          Image.asset(
                                            YOUTUBE_ICON,
                                            height: 30,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 2,
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.email_outlined,
                                            size: 18,
                                          ),
                                          Text(
                                            "info@stayalert.pk",
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.language,
                                            size: 18,
                                          ),
                                          Text(
                                            "www.stayalert.pk",
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ], //<Widget>[]
                            mainAxisAlignment: MainAxisAlignment.center,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
