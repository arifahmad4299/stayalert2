import 'package:flutter/material.dart';
import 'package:stayalert/helpers/constants.dart';
import '../custom_widgets/common_app_bar.dart';

class Assignment extends StatefulWidget {
  @override
  _AssignmentState createState() => _AssignmentState();
}

class _AssignmentState extends State<Assignment> {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              commonAppBar(
                  shadowColor: Colors.grey.shade300,
                  screenContext: context,
                  profilePic: false),
              SizedBox(
                height: 25,
              ),
              FractionallySizedBox(
                widthFactor: 0.90,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'HOME WORK',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: HEADING_FONT),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(20),
                        ),
                        color: Colors.white,
                        boxShadow: [
                          new BoxShadow(
                            spreadRadius: 2,
                            offset: Offset(0, 5),
                            color: Colors.grey.shade300,
                            blurRadius: 5,
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Today Home Work',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: TITLE_FONT),
                            ),
                            SizedBox(height: 20),
                            Text(
                              'Dated: 05/05/2021',
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: TITLE_FONT),
                            ),
                            SizedBox(height: 20),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: size.width / 3.5,
                                  height: 8,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Colors.green),
                                ),
                                Text(
                                  'Class 7th Section: B',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: TITLE_FONT),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    'Never give up, ',
                                    style: TextStyle(fontSize: TITLE_FONT),
                                  ),
                                  Text(
                                    'know more',
                                    style: TextStyle(
                                        color: Colors.yellow[600],
                                        fontSize: TITLE_FONT),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    child: Icon(Icons.arrow_forward,
                                        color: Colors.black, size: 30.0),
                                    width: 50.0,
                                    height: 50.0,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 5,
                                          color: Colors.grey,
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
