import 'package:flutter/material.dart';
import 'package:stayalert/helpers/constants.dart';
import 'package:stayalert/views/announcement.dart';
import 'package:stayalert/views/assignment.dart';
import 'package:stayalert/views/attendance.dart';
import 'package:stayalert/views/fee_reminder_screen.dart';
import 'package:stayalert/views/result_card.dart';
import 'package:stayalert/views/time_table_screen.dart';
import '../custom_widgets/bottom_nav_bar.dart';
import '../custom_widgets/common_app_bar.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool _arrowFlag = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              color: Colors.grey[800],
              child: Column(
                children: [
                  commonAppBar(
                      screenContext: context,
                      shadowColor: Colors.grey.shade800,
                      profilePic: true),
                  SizedBox(
                    height: 30,
                  ),
                  FractionallySizedBox(
                    widthFactor: 0.85,
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.white),
                      height: 100,
                      child: FractionallySizedBox(
                        widthFactor: 0.90,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 12,
                                ),
                                Text("Name:"),
                                SizedBox(
                                  height: 7,
                                ),
                                Text("F/Name:"),
                                SizedBox(
                                  height: 7,
                                ),
                                Text("Roll No:")
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 12,
                                ),
                                Text(
                                  "Mark Stephen",
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                                SizedBox(
                                  height: 7,
                                ),
                                Text(
                                  "John Kennedy",
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                                SizedBox(
                                  height: 7,
                                ),
                                Text(
                                  "14",
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 12,
                                ),
                                Text("Class:"),
                                SizedBox(
                                  height: 7,
                                ),
                                Text("Section:")
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 12,
                                ),
                                Text(
                                  "10th",
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                                SizedBox(
                                  height: 7,
                                ),
                                Text(
                                  "D",
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20)),
                      color: Colors.white,
                    ),
                    child: FractionallySizedBox(
                      widthFactor: 0.90,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 25,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              homeDashboard(
                                  imagePath: ATTENDANCE_ICON,
                                  title: "Attendance"),
                              homeDashboard(
                                  imagePath: ASSIGNMENT_ICON,
                                  title: "Assignments"),
                              homeDashboard(
                                  imagePath: TIME_TABLE_ICON,
                                  title: "Time Table"),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              homeDashboard(
                                  imagePath: RESULT_ICON, title: "Results"),
                              homeDashboard(
                                  imagePath: FEE_PAYMENT_ICON, title: "Fees"),
                              homeDashboard(
                                  imagePath: NOTICE_ICON,
                                  title: "Notice Board"),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 8.0, right: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                InkWell(
                                  onTap: () {
                                    if (_arrowFlag == false) {
                                      setState(() {
                                        _arrowFlag = true;
                                      });
                                    } else {
                                      setState(() {
                                        _arrowFlag = false;
                                      });
                                    }
                                  },
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Recent Activities",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      InkWell(
                                        child: Icon(
                                          _arrowFlag == false
                                              ? Icons.expand_more
                                              : Icons.expand_less,
                                          size: 25,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Text("Today, 23 June 2021")
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          _arrowFlag == false
                              ? Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(20),
                                    ),
                                    color: Colors.white,
                                    boxShadow: [
                                      new BoxShadow(
                                        spreadRadius: 2,
                                        offset: Offset(0, 5),
                                        color: Colors.grey.shade300,
                                        blurRadius: 5,
                                      ),
                                    ],
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 15.0, bottom: 15.0),
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Fee Reminder",
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              SizedBox(
                                                height: 20,
                                              ),
                                              Text(
                                                "Last Date:",
                                              ),
                                              SizedBox(
                                                height: 20,
                                              ),
                                              Text("23/May/2021")
                                            ],
                                          ),
                                          SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                5,
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Status",
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              SizedBox(
                                                height: 20,
                                              ),
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(4.0),
                                                  child: Text(
                                                    "UnPaid",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                ),
                                                decoration: BoxDecoration(
                                                    color: Colors.red,
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                6))),
                                              )
                                            ],
                                          ),
                                        ]),
                                  ),
                                )
                              : Container(),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(20),
                              ),
                              color: Colors.white,
                              boxShadow: [
                                new BoxShadow(
                                  spreadRadius: 2,
                                  offset: Offset(0, 5),
                                  color: Colors.grey.shade300,
                                  blurRadius: 5,
                                ),
                              ],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 15.0, bottom: 15.0),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Advertisement",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          "Welcome to Ling Lang Foundation School",
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                            "We teach you which help in both the world!")
                                      ],
                                    ),
                                    SizedBox(
                                      width:
                                          MediaQuery.of(context).size.width / 5,
                                    ),
                                  ]),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Events and Notifications",
                              style: TextStyle(
                                  fontSize: TITLE_FONT,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(20),
                              ),
                              color: Colors.white,
                              boxShadow: [
                                new BoxShadow(
                                  spreadRadius: 2,
                                  offset: Offset(0, 5),
                                  color: Colors.grey.shade300,
                                  blurRadius: 5,
                                ),
                              ],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Everyday English-French French',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontSize: TITLE_FONT),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            'Conversation and Fun - Joe!',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontSize: TITLE_FONT),
                                          ),
                                        ],
                                      ),
                                      Image.asset(NOTICE_ICON,
                                          width: 84, height: 85),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        '9 hrs',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: TITLE_FONT),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Align(
                                    alignment: Alignment.bottomRight,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(
                                          'Never give up, ',
                                          style:
                                              TextStyle(fontSize: TITLE_FONT),
                                        ),
                                        Text(
                                          'know more',
                                          style: TextStyle(
                                              color: Colors.yellow[600],
                                              fontSize: TITLE_FONT),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Container(
                                          child: Icon(Icons.arrow_forward,
                                              color: Colors.black, size: 30.0),
                                          width: 50.0,
                                          height: 50.0,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            shape: BoxShape.circle,
                                            boxShadow: [
                                              BoxShadow(
                                                blurRadius: 5,
                                                color: Colors.grey,
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            height: 40,
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget homeDashboard({required String imagePath, required String title}) {
    return InkWell(
      onTap: () {
        switch (title) {
          case "Attendance":
            {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => Attendance()));
            }
            break;
          case "Assignments":
            {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => Assignment()));
            }
            break;
          case "Time Table":
            {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => TimeTableScreen()));
            }
            break;

          case "Results":
            {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => ResultScreen()));
            }
            break;
          case "Fees":
            {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => FeeReminderScreen()));
            }
            break;
          case "Notice Board":
            {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => Announcement()));
            }
            break;
          default:
        }
      },
      child: Container(
        height: 95,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Image.asset(
              imagePath,
              width: 100,
            ),
            SizedBox(
              height: 3,
            ),
            Text(title)
          ],
        ),
      ),
    );
  }
}
