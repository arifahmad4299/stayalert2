import 'package:flutter/material.dart';
import 'package:stayalert/helpers/constants.dart';
import '../views/verifycode_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final phoneNumber = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              color: Colors.white,
              child: Column(
                children: [
                  SizedBox(
                    height: 3,
                  ),
                  Center(
                    child: Image.asset(
                      STAY_ALERT_LOGO,
                      height: 80,
                      width: MediaQuery.of(context).size.width / 2,
                    ),
                  ),
                  SizedBox(
                    height: 100,
                  ),
                  Center(
                    child: Image.asset(
                      LOGIN_ICON,
                      height: 143,
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 1.66,
                    decoration: BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 50,
                        ),
                        Text(
                          "Welcome to Stay Alert School",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: HEADING_FONT),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          "Please Enter Your Mobile No Below",
                          style: TextStyle(fontSize: TITLE_FONT),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5)),
                          width: 165,
                          child: Center(
                            child: TextFormField(
                                controller: phoneNumber,
                                // initialValue: "+92",
                                textAlign: TextAlign.center,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                )),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            String mobileNumber = phoneNumber.text;
                            print(mobileNumber + "/////");
                            RegExp reGex = RegExp(r'(^(?:[+0]9)?[0-9]{10}$)');
                            print(reGex.hasMatch(mobileNumber));
                            if (mobileNumber.toString() == "" ||
                                mobileNumber.isEmpty ||
                                reGex.hasMatch(mobileNumber) == false) {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(const SnackBar(
                                backgroundColor: Colors.red,
                                content: Text(
                                  'Please enter valid Mobile number!',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ));
                            } else {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => OTPScreen()));
                            }
                          },
                          child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.black,
                                  borderRadius: BorderRadius.circular(5)),
                              width: 165,
                              height: 42,
                              child: Center(
                                child: Text(
                                  "Login",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: TITLE_FONT),
                                ),
                              )),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height / 9,
                        ),
                        Container(
                          height: 100,
                          child: Row(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width / 2,
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Column(
                                    children: [
                                      Text(
                                        "Please Like Us On!",
                                        style: TextStyle(color: Colors.black),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            FACEBOOK_ICON,
                                            height: 30,
                                          ),
                                          SizedBox(
                                            width: 3,
                                          ),
                                          Image.asset(
                                            TWITTER_ICON,
                                            height: 30,
                                          ),
                                          SizedBox(
                                            width: 3,
                                          ),
                                          Image.asset(
                                            INSTAGRAM_ICON,
                                            height: 30,
                                          ),
                                          SizedBox(
                                            width: 3,
                                          ),
                                          Image.asset(
                                            YOUTUBE_ICON,
                                            height: 30,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 2,
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(Icons.email_outlined,
                                              size: ICON_FONT),
                                          Text(
                                            "info@stayalert.pk",
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(Icons.language, size: ICON_FONT),
                                          Text(
                                            "www.stayalert.pk",
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ], //<Widget>[]
                            mainAxisAlignment: MainAxisAlignment.center,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
