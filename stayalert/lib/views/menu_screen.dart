import 'package:flutter/material.dart';
import 'package:stayalert/custom_widgets/common_app_bar.dart';

class MenuScreen extends StatefulWidget {
  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Column(
              children: [
                commonAppBar(
                    shadowColor: Colors.grey.shade300,
                    screenContext: context,
                    profilePic: true),
                Container(
                  height: 30,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
