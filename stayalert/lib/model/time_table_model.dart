class TimeTableModel {
  String subjectName;
  String time;
  String? standard;
  String? section;
  TimeTableModel(
      {required this.subjectName,
      required this.time,
      this.standard,
      this.section});
}